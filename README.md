Spring 2019 Meeting Schedule

1/18	FIRST WEEK OF SCHOOL NO CLUB MEETING

1/24	Big picture

1/31    Command Line

2/7     File Perms / SSH

2/14	Firewalls

2/21	Scanning & Enumeration

2/18 	Backdoors

3/7     SPRING BREAK

3/17	Apache web server (or Nginx)

3/21	Vulnerable web apps

3/28	Vulnerable Web apps

4/4     Exploitation

4/11	 Persistence

4/18	Password Cracking

4/25	Bash Scripting

5/5-5/11	FINALS WEEK


--------------------------------------

Advanced Topics: Wednesdays at 4:30pm

1/13-1/19	FIRST WEEK NO CLUB MEETINGS

1/20-2/2	More Persistence

2/3-2/9	    More Persistence

2/10-2/16	Stack Overflows

2/17-2/23	Stack Overflows

2/24-3/2	Reverse Engineering

3/3-3/9	    Reverse Engineering

3/10-3/16	SPRING BREAK

3/17-3/23	Log monitoring

3/24-3/30	Network analysis / wifi hacking

3/31-4/6	Tool spotlights

4/7-4/13	Social Engineering

4/14-4/20	Adv. Privesc (Linux)

4/21-4/27	Adv. Privesc (Windows)

4/28-5/4	FINALS WEEK STARTS

5/5-5/11	FINALS WEEK
